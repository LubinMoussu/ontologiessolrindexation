
from rdflib import Graph, ConjunctiveGraph, Literal, BNode, Namespace, RDF, URIRef, term
from rdflib.namespace import DC, FOAF, OWL, RDF, RDFS
import json
import requests
# from typing import List, NamedTuple, Optional, Dict, TypeVar

import pysolr
from pprint import pprint
import os

class IndexDoc():

    def __init__(self,
                 id=str,
                 **kwargs
                 ):
        self.id = id
        for k, v in kwargs.items():
            setattr(self, k, v)

    def convert_asjson(self, *args, **kwargs) -> str:
        doc_dict = {k: v
                    for k, v in vars(self).items()
                    if v}
        return json.dumps(doc_dict, *args, **kwargs)

class SolrInstance():

    def __init__(self,
                 solr_config: dict,
                 valid_fields: list,
                 ):

        self.solr_instance = pysolr.Solr(solr_config['url'],
                                         timeout=solr_config['timeout'])
        self.solr = solr_config['url']

    def inject_into_solr(self,
                         docs: list) -> None:

        path = self.solr + '/update'
        params = {
            'commit': 'true'
        }
        headers = {
            'Content-type': 'application/json'
        }
        pprint(docs[:10])
        print('len_docs:', len(docs))
        try:
            solr_req = requests.post(path,
                                     params=params,
                                     data=json.dumps(docs),
                                     headers=headers)
            solr_response = solr_req.json()
            print('solr_response: ', solr_response)
        except Exception as ex:
            print('SolrInstance.inject_into_solr: {0}'.format(ex))

    def solr_searches(self):

        results = self.solr_instance.search('*:*', limit=100, rows=14090)
        print("Saw {0} result(s).".format(len(results)))
        print('------------------------')
        results = self.solr_instance.search(q='synonyms:carcinoma, diffuse type', rows=14090)
        print("Saw {0} result(s).".format(len(results)))
        pprint(list(results))

class LoadOnto():

    def __init__(self):
        self.graph = Graph()

    def get_ontology_classes(self) -> 'generator':

        list_pred_object_tuple = [(RDF.type, OWL.Class),
                                  (RDFS.range, None),
                                  (RDF.type, RDFS.Class),
                                  (RDFS.subClassOf, None),
                                  (RDFS.domain, None)]

        for pred_object_tuple in list_pred_object_tuple:
            for subj in self.graph.subjects(pred_object_tuple[0], pred_object_tuple[1]):
                label = str(self.graph.label(subj))
                if type(subj) == URIRef and label:
                    yield (subj, label, os.path.basename(subj))

    def get_synonyms(self) -> 'generator':

        list_predicate = [URIRef('http://www.geneontology.org/formats/oboInOwl#hasExactSynonym'),
                          URIRef('http://www.geneontology.org/formats/oboInOwl#hasRelatedSynonym'),
                          URIRef('http://www.geneontology.org/formats/oboInOwl#hasNarrowSynonym'),
                          URIRef('http://www.geneontology.org/formats/oboInOwl#hasBroadSynonym')]

        for predicate in list_predicate:
            for subj, obj in self.graph.subject_objects(predicate):
                yield (subj, obj)

    def load_ontology_graph(self,
                            file: str):
        self.graph.parse(file,
                         format='xml')

    def build_index_file(self,
                         valid_fields: list,
                         concerned_field: str,
                         ) -> 'generator':

        # load ontology_classes
        subj_label_gen = list(set(self.get_ontology_classes()))
        print('subj_label_gen', len(subj_label_gen))

        # load synosyms
        subj_synonym_obj = self.get_synonyms()
        subj_synonym_dict = {}
        for k, v in subj_synonym_obj:
            try:
                subj_synonym_dict[k].append(v)
            except KeyError:
                subj_synonym_dict[k] = [v]

        solr_doc_list = []
        for subj_label in subj_label_gen:
            subj, label, basename = subj_label

            field_values = {field: None
                            for field in valid_fields
            }
            field_values[concerned_field] = label
            field_values['synonyms'] = subj_synonym_dict[subj] if subj in subj_synonym_dict else None

            doc = IndexDoc(id=basename,
                           phenotype_terms=field_values['phenotype_terms'],
                           study_terms=field_values['study_terms'],
                           gene_terms=field_values['gene_terms'],
                           disease_terms=field_values['disease_terms'],
                           synonyms=field_values['synonyms'],
            )

            yield json.loads(doc.convert_asjson())

    def inject_ontology_classes(self,
                                solr_instance,
                                valid_fields: list,
                                concerned_field: str):

        solr_doc_list = self.build_index_file(valid_fields,
                                              concerned_field)
        if solr_doc_list:
            solr_instance.inject_into_solr(list(solr_doc_list))


if __name__ == "__main__":

    solr_config = {'url': 'http://192.168.56.1:8983/solr/go', 'timeout': 10}
    valid_fields = ['phenotype_terms', 'study_terms', 'gene_terms', 'disease_terms', 'synonyms']

    onto_file_dict = {
        'phenotype_terms': 'Inputs/hp.owl',
        'study_terms': 'Inputs/StudyDesigns.owl',
        'gene_terms': 'http://snapshot.geneontology.org/ontology/go.owl',
        'disease_terms': 'Inputs/doid.owl'
    }
    onto_file_dict = {
        'disease_terms': 'Inputs/doid.owl',
    }

    solr_instance = SolrInstance(solr_config, valid_fields)
    # solr_instance.solr_searches()

    for onto in onto_file_dict:
        print('onto_file', onto_file_dict[onto])
        loader = LoadOnto()
        loader.load_ontology_graph(onto_file_dict[onto])

        loader.inject_ontology_classes(solr_instance,
                                       valid_fields,
                                       onto)
        print('-------------------------------')